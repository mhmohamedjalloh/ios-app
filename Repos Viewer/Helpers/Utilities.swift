//
//  Utilities.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class Utilites {

    static func isValidEmailAddress(emailAddressString: String) -> Bool {
          
          var returnValue = true
          let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
          
          do {
              let regex = try NSRegularExpression(pattern: emailRegEx)
              let nsString = emailAddressString as NSString
              let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
              
              if results.count == 0
              {
                  returnValue = false
              }
              
          } catch let error as NSError {
              print("invalid regex: \(error.localizedDescription)")
              returnValue = false
          }
          
          return  returnValue
      }
    
       class UIHelper {
            
            private static var instance = UIHelper ()
            
            static func shared () -> UIHelper {
                return instance
            }
        
        func showSingleButtonAlert(title: String, message: String, controller: UIViewController, handler: ((UIAlertAction) -> Void)? = nil){
            // create the alert
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: handler))
            
            
            // show the alert
            controller.present(alert, animated: true, completion: nil)
        }
        
            
            func startActivityIndicatorFor( viewController: UIViewController) {
                viewController.view.activityStartAnimating(activityColor: UIColor.white, backgroundColor: UIColor.black.withAlphaComponent(0.5))
            }
    
        
    }

    
    
}
