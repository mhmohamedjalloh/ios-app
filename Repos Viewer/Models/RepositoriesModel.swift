//
//  RepositoriesModel.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import Foundation

struct RepositoriesModel: Codable {
    let pagelen: Int
    let values: [Repo]
    let page, size: Int
    
}

struct Repo: Codable {
    let scm: String
    let website: String?
    let hasWiki: Bool
    let uuid: String
    let links: ValueLinks
    let forkPolicy, fullName, name: String
    let project: Project
    let language, createdOn: String
    let mainbranch: Mainbranch
    let workspace: Project
    let hasIssues: Bool
    let owner: Owner
    let updatedOn: String
    let size: Int
    let type, slug: String
    let isPrivate: Bool
    let description: String

    enum CodingKeys: String, CodingKey {
        case scm, website
        case hasWiki = "has_wiki"
        case uuid, links
        case forkPolicy = "fork_policy"
        case fullName = "full_name"
        case name, project, language
        case createdOn = "created_on"
        case mainbranch, workspace
        case hasIssues = "has_issues"
        case owner
        case updatedOn = "updated_on"
        case size, type, slug
        case isPrivate = "is_private"
        case description
    }
}

struct ValueLinks: Codable {
    let watchers, branches, tags, commits: Avatar
    let clone: [Clone]
    let purpleSelf, source, html, avatar: Avatar
    let hooks, forks, downloads, pullrequests: Avatar

    enum CodingKeys: String, CodingKey {
        case watchers, branches, tags, commits, clone
        case purpleSelf = "self"
        case source, html, avatar, hooks, forks, downloads, pullrequests
    }
}

struct Avatar: Codable {
    let href: String
}

struct Clone: Codable {
    let href: String
    let name: Name
}

enum Name: String, Codable {
    case https = "https"
    case ssh = "ssh"
}

struct Mainbranch: Codable {
    let type, name: String
}

struct Owner: Codable {
    let displayName, uuid: String
    let links: OwnerLinks
    let type, nickname, accountID: String

    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case uuid, links, type, nickname
        case accountID = "account_id"
    }
}

struct OwnerLinks: Codable {
    let purpleSelf, html, avatar: Avatar

    enum CodingKeys: String, CodingKey {
        case purpleSelf = "self"
        case html, avatar
    }
}

struct Project: Codable {
    let links: OwnerLinks
    let type: PurpleType
    let name: String
    let key: String?
    let uuid: String
    let slug: String?
}

enum PurpleType: String, Codable {
    case project = "project"
    case workspace = "workspace"
}

// MARK: Convenience initializers

extension RepositoriesModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(RepositoriesModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Repo {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Repo.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension ValueLinks {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(ValueLinks.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Avatar {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Avatar.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Clone {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Clone.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Mainbranch {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Mainbranch.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Owner {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Owner.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension OwnerLinks {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(OwnerLinks.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Project {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Project.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable {
    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
