//
//  ApiResponseHandler.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import Foundation

@objc protocol APIResponseHanlder {
    func handleAPIResponse (isSucceed: Bool, apiAction: String)
    @objc optional func handleResponsee (isSucceed: Bool)
    @objc optional func updateProgress (with percentage: Double)
    @objc func forceLogout (animated: Bool)
}
