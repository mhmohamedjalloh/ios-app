//
//  RepoDetailsViewControllers.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import UIKit

class RepoDetailsViewControllers: UIViewController {
    
    // MARK:- Variables
    var repoData : Repo?
    

    // MARK:- IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var isPrivateLabel: UILabel!
    
    
    
    // MARK:- IBActions

    
    override func viewDidLoad() {
        initViews()
    }
    
    func initViews() {
        nameLabel.text = repoData?.name ?? ""
        projectNameLabel.text = repoData?.project.name ?? ""
        ownerLabel.text = repoData?.owner.displayName
        isPrivateLabel.text = (repoData?.isPrivate ?? false ) ? "Yes" : "No"
    }
}
