//
//  LoginViewController.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var showPasswordBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    
    // MARK: - IBActions Callbacks
    
    @IBAction func showPasswordBtnTapped(_ sender: Any) {
            passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        if isDataValid() {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarViewController") as! TabBarViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    // MARK: - Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextField {
                if(text.count > 0)  || textfield.tag == 1{
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
    }

    // MARK: - UIViewController Callbacks

    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        initViews()
    }
    
    
    
    // MARK: - Utility Methods
    
    func initViews() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func isDataValid() -> Bool {
        var isValid = true
        
        if !Utilites.isValidEmailAddress(emailAddressString: emailTextField.text!) {
            isValid = false
            emailTextField.errorMessage = "Invalid Email Address"
            return isValid
        }
        
        if passwordTextField.text!.count < 8 {
            isValid = false
            passwordTextField.errorMessage = "Password is too short"
            return isValid
        }
        
        if emailTextField.text! != "user@xyz.com" || passwordTextField.text! != "11111111aA@" {
            isValid = false
            Utilites.UIHelper.shared().showSingleButtonAlert(title: "", message: "This user doesn't exist", controller: self)
            return isValid
        }
        
        return isValid
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
