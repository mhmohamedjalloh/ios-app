//
//  SecondViewController.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import UIKit

class SettingsViewController: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var accountIdLabel: UILabel!
    
    
    // MARK:- IBActions
    @IBAction func logOutBtnTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clearSessionData"), object: nil, userInfo: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNav")
        self.present(vc, animated: true)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    func initViews() {
        if let ownerData = GetallRepositoriesViewModel.shared().resultsModel?.values.first?.owner {
            displayNameLabel.text = ownerData.displayName
            typeLabel.text = ownerData.type
            nickNameLabel.text = ownerData.nickname
            accountIdLabel.text = ownerData.accountID
        }
    }


}

