//
//  FirstViewController.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import UIKit
import Foundation

class RepositoriesViewController: UIViewController, APIResponseHanlder {
    
    // MARK: - IBOutlets
    @IBOutlet weak var repositoriesTableView: UITableView!
    
    
    func handleAPIResponse(isSucceed: Bool, apiAction: String) {
        self.view.activityStopAnimating()
        
        if apiAction == Constants.ACTION_GET_ALL_REPOSITORIES {
            if isSucceed {
                self.repositoriesTableView.reloadData()
            }
        }
    }
    
    func forceLogout(animated: Bool) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        Utilites.UIHelper.shared().startActivityIndicatorFor(viewController: self)
        GetallRepositoriesViewModel.shared().getRepositories(handler: self)
        repositoriesTableView.dataSource = self
        repositoriesTableView.delegate = self
    }


}


extension RepositoriesViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GetallRepositoriesViewModel.shared().resultsModel?.size ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repositoriesCell") as! RepositoriesTableViewCell
        cell.repositoryName.text = GetallRepositoriesViewModel.shared().resultsModel?.values[indexPath.row].fullName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repoData = GetallRepositoriesViewModel.shared().resultsModel?.values[indexPath.row]
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "repoDetailsViewControllers") as! RepoDetailsViewControllers
        vc.repoData = repoData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

