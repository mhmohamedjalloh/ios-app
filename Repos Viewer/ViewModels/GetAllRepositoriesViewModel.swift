//
//  GetAllRepositoriesViewModel.swift
//  Repos Viewer
//
//  Created by Mohamed Hamed on 9/18/21.
//

import Foundation
import Alamofire

class GetallRepositoriesViewModel {
    
    var resultsModel : RepositoriesModel?
    var responseHandler : APIResponseHanlder?
    var message : String?
    
    private static var instance = GetallRepositoriesViewModel()
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.clearData), name: NSNotification.Name(rawValue: "clearSessionData"), object: nil)
    }
    
    func getRepositories(handler : APIResponseHanlder){
        
        self.responseHandler = handler
        //        UIHelper.shared().toggleLoadingDialog(show: true)
        
        
        let username = "mohamed_hamed4158@yahoo.com"
        let password = "88888888mM_"
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Basic \(base64Credentials)"
        ]
        
        AF.request(Constants.ACTION_GET_ALL_REPOSITORIES, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseDecodable { (response: AFDataResponse<RepositoriesModel>) in
                
                self.resultsModel = response.value
                
                print(response.debugDescription)
                
                
                if self.resultsModel != nil {
                    self.responseHandler?.handleAPIResponse(isSucceed: true, apiAction: Constants.ACTION_GET_ALL_REPOSITORIES)
                } else {
                    self.responseHandler?.handleAPIResponse(isSucceed: false, apiAction: Constants.ACTION_GET_ALL_REPOSITORIES)
                }
                
        }
    }
    
    public static func shared() -> GetallRepositoriesViewModel {
        return instance
    }
    
    @objc func clearData () {
        self.resultsModel = nil
    }
    
}





